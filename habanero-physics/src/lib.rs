use jalapeno::math::{
    topology::{VectorSpace, XyzVector},
    vector::Vector,
    Field,
};
pub mod world;

pub const fn right<T: Field<T>, S: VectorSpace<3, Coordinate = T, Coordinates = XyzVector<T>>>(
) -> Vector<S, 3> {
    Vector::<S, 3>::OX
}

pub const fn forward<T: Field<T>, S: VectorSpace<3, Coordinate = T, Coordinates = XyzVector<T>>>(
) -> Vector<S, 3> {
    Vector::<S, 3>::OY
}

pub const fn up<T: Field<T>, S: VectorSpace<3, Coordinate = T, Coordinates = XyzVector<T>>>(
) -> Vector<S, 3> {
    Vector::<S, 3>::OZ
}
