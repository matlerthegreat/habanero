use std::collections::HashMap;

use jalapeno::math::{
    point::Point,
    topology::{Digital, MetricSpace},
    vector::Vector,
};

use self::{
    chunk::Chunk,
    topology::{ChunkSpace, WorldSpace},
    voxel::Voxel,
};

pub mod chunk;
pub mod topology;
pub mod voxel;

#[derive(Debug, Default)]
pub struct World {
    pub chunks: HashMap<Point<Digital, 3>, Chunk>,
}

impl World {
    pub fn new() -> Self {
        let mut world = World::default();
        let radius = 32768;
        world.generate_sphere(Point([radius; 3]), 16);
        world.generate_hills(4096);
        world
    }

    fn generate_sphere(&mut self, center: Point<WorldSpace, 3>, radius: i32) {
        let min = center - Vector([radius; 3]);
        let max = center + Vector([radius; 3]);
        for x in min.x..max.x {
            for y in min.y..max.y {
                for z in min.z..max.z {
                    let p = Point([x, y, z]);
                    if WorldSpace::distance(center, p) < radius {
                        let (c, v) = ChunkSpace::from_world_space(p);
                        self.chunks.entry(c).or_default()[v].color = 16;
                    }
                }
            }
        }
    }

    fn generate_hills(&mut self, dimension: i32) {
        let period = 16.0;
        let amplitude = 8.0;
        for x in 0..dimension {
            for y in 0..dimension {
                let z = 32768
                    + ((x as f32 / period).sin() + (y as f32 / period).cos() * amplitude) as i32;
                let p = Point([x, y, z]);
                let (c, v) = ChunkSpace::from_world_space(p);
                self.chunks.entry(c).or_default()[v].color = 16;
            }
        }
    }

    pub fn get_voxel(&self, p: Point<WorldSpace, 3>) -> Option<&Voxel> {
        let (c, v) = ChunkSpace::from_world_space(p);
        self.chunks.get(&c).map(|chunk| &chunk[v])
    }

    pub fn is_transparent(&self, v: Point<WorldSpace, 3>) -> bool {
        self.get_voxel(v).map(|v| v.color == 0).unwrap_or(true)
    }

    pub fn is_surface(&self, p: Point<WorldSpace, 3>) -> bool {
        for dx in [-1, 1] {
            if self.is_transparent(p + dx * Vector::<WorldSpace, 3>::OX) {
                return true;
            }
        }
        for dy in [-1, 1] {
            if self.is_transparent(p + dy * Vector::<WorldSpace, 3>::OY) {
                return true;
            }
        }
        for dz in [-1, 1] {
            if self.is_transparent(p + dz * Vector::<WorldSpace, 3>::OZ) {
                return true;
            }
        }

        false
    }
}
