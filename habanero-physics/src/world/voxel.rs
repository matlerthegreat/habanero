#[derive(Debug, Clone)]
pub struct Voxel {
    pub color: VoxelColor,
}

pub type VoxelColor = u16;
