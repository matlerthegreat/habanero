use std::ops::{Index, IndexMut};

use jalapeno::math::point::Point;

use super::{topology::ChunkSpace, voxel::Voxel};

#[derive(Debug)]
pub struct Chunk {
    voxels: [Voxel; Self::VOLUME],
}

impl Chunk {
    pub const DIMENSION: usize = 1 << 5;
    pub const VOLUME: usize = Self::DIMENSION * Self::DIMENSION * Self::DIMENSION;

    pub fn keys() -> impl Iterator<Item = Point<ChunkSpace, 3>> {
        (0..Self::DIMENSION).flat_map(move |x| {
            (0..Self::DIMENSION).flat_map(move |y| {
                (0..Self::DIMENSION)
                    .map(move |z| Point::<ChunkSpace, 3>([x as u32, y as u32, z as u32]))
            })
        })
    }

    pub fn iter(&self) -> impl Iterator<Item = (Point<ChunkSpace, 3>, &Voxel)> {
        Self::keys().map(|k| (k, &self[k]))
    }
}

impl Default for Chunk {
    fn default() -> Self {
        Self {
            voxels: std::array::from_fn(|_| Voxel { color: 0 }),
        }
    }
}

impl Index<Point<ChunkSpace, 3>> for Chunk {
    type Output = Voxel;

    fn index(&self, v: Point<ChunkSpace, 3>) -> &Self::Output {
        let i = (v.x * Self::DIMENSION as u32 * Self::DIMENSION as u32
            + v.y * Self::DIMENSION as u32
            + v.z) as usize;
        &self.voxels[i]
    }
}

impl IndexMut<Point<ChunkSpace, 3>> for Chunk {
    fn index_mut(&mut self, v: Point<ChunkSpace, 3>) -> &mut Self::Output {
        let i = (v.x * Self::DIMENSION as u32 * Self::DIMENSION as u32
            + v.y * Self::DIMENSION as u32
            + v.z) as usize;
        &mut self.voxels[i]
    }
}
