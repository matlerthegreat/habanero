use std::borrow::Borrow;

use jalapeno::{
    componentwise_vector_space_impl,
    math::{
        point::Point,
        topology::{Digital, InnerProductSpace, MetricSpace, Space, VectorSpace, XyzVector},
        Integer,
    },
};

use super::chunk::Chunk;

// Relative chunk-voxel space
pub struct ChunkSpace;

impl ChunkSpace {
    pub fn from_world_space(p: Point<WorldSpace, 3>) -> (Point<Digital, 3>, Point<ChunkSpace, 3>) {
        let chunk = Point([
            (p.x / Chunk::DIMENSION as i32),
            (p.y / Chunk::DIMENSION as i32),
            (p.z / Chunk::DIMENSION as i32),
        ]);
        let voxel = Point([
            u32::try_from(p.x.rem_euclid(Chunk::DIMENSION as i32)).unwrap(),
            u32::try_from(p.y.rem_euclid(Chunk::DIMENSION as i32)).unwrap(),
            u32::try_from(p.z.rem_euclid(Chunk::DIMENSION as i32)).unwrap(),
        ]);
        (chunk, voxel)
    }
}

impl Space<3> for ChunkSpace {
    type Coordinate = u32;
    type Coordinates = XyzVector<u32>;

    fn as_coordinates(src: &[Self::Coordinate; 3]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 3]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}

// Absolute world-voxel space
pub struct WorldSpace;
impl Space<3> for WorldSpace {
    type Coordinate = Integer;
    type Coordinates = XyzVector<Integer>;

    fn as_coordinates(src: &[Self::Coordinate; 3]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 3]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}

componentwise_vector_space_impl!(over Integer, for WorldSpace, 3);
impl InnerProductSpace<3> for WorldSpace {}

impl MetricSpace<3> for WorldSpace {
    fn length_squared(v: impl Borrow<[Self::Coordinate; 3]> + Copy) -> Self::F {
        Self::length(v).pow(2)
    }

    fn length(v: impl Borrow<[Self::Coordinate; 3]>) -> Self::F {
        v.borrow().iter().map(|i| i.abs()).sum()
    }
}
