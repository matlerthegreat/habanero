use anyhow::Result;
use std::{
    num::NonZeroU32,
    sync::{Arc, RwLock},
};
use winit::{dpi::PhysicalSize, window::Window};

use crate::scene::Scene;

use self::raytracing::render;

mod octree_traversal;
mod raytracing;

pub struct Renderer {
    surface: softbuffer::Surface<Arc<Window>, Arc<Window>>,
    width: u32,
    height: u32,
    scene: Arc<RwLock<Scene>>,
}

impl Renderer {
    pub async fn new(window: Arc<Window>, scene: Arc<RwLock<Scene>>) -> Self {
        let (width, height) = {
            let size = window.inner_size();

            (size.width, size.height)
        };

        let context = softbuffer::Context::new(window.clone()).unwrap();
        let mut surface = softbuffer::Surface::new(&context, window).unwrap();

        surface
            .resize(
                NonZeroU32::new(width).unwrap(),
                NonZeroU32::new(height).unwrap(),
            )
            .unwrap();

        Self {
            width,
            height,
            surface,
            scene,
        }
    }

    pub fn resize(&mut self, new_size: PhysicalSize<u32>) {
        let (width, height) = { (new_size.width, new_size.height) };
        if width > 0 && height > 0 {
            self.surface
                .resize(
                    NonZeroU32::new(width).unwrap(),
                    NonZeroU32::new(height).unwrap(),
                )
                .unwrap();
            self.width = width;
            self.height = height;
        }
    }

    pub fn render(&mut self) -> Result<()> {
        let mut buffer = self.surface.buffer_mut().unwrap();

        {
            let scene = self.scene.read().unwrap();
            render(self.width, self.height, &scene, &mut buffer)
        }

        buffer.present().unwrap();

        Ok(())
    }
}
