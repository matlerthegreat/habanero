use std::{array, cmp::min, mem, num::NonZeroU16, slice};

use jalapeno::math::point::Point;

use super::{topology::ScreenSpace, Scene};

#[derive(Debug)]
pub struct Octree<T> {
    nodes: Vec<Node<T>>,
    merger: fn([&T; 8]) -> T,
}

pub type OctreeIndex = NonZeroU16;

#[repr(transparent)]
#[derive(Debug)]
pub struct Node<T> {
    pub children: [(T, Option<OctreeIndex>); 8],
}

impl<T> Octree<T> {
    pub fn get(&self, gen: usize, sibling: usize) -> &(T, Option<OctreeIndex>) {
        &self.nodes[gen].children[sibling]
    }

    pub fn get_raw(&self) -> &[u8] {
        unsafe {
            let data = self.nodes.as_ptr() as *const u8;
            let len = self.nodes.len() * mem::size_of::<Node<T>>();
            slice::from_raw_parts(data, len)
        }
    }

    pub fn size(&self) -> usize {
        self.nodes.len()
    }
}

impl<T: Default + Clone> Octree<T> {
    pub fn new(merger: fn([&T; 8]) -> T) -> Self {
        let nodes = Vec::new();

        Self { nodes, merger }
    }

    pub fn insert(&mut self, p: Point<ScreenSpace, 3>, depth: usize, value: T) {
        let node = if self.nodes.first().is_some() {
            0
        } else {
            let node = Node {
                children: array::from_fn(|_| (T::default(), None)),
            };
            self.nodes.push(node);
            0
        };

        let l =
            p.0.iter()
                .map(|i| ((*i as i32).abs_diff(Scene::RADIUS)))
                .max()
                .unwrap() as f32;

        let limit = depth.saturating_sub((l.log2()) as usize) + 4;
        let limit = min(depth, limit);

        self.insert_rec(p, depth, limit, value, 1, node);
    }

    fn insert_rec(
        &mut self,
        p: Point<ScreenSpace, 3>,
        depth: usize,
        limit: usize,
        value: T,
        d: usize,
        node: usize,
    ) -> T {
        let i = (((p.x >> (depth - d)) & 1) * 4
            + ((p.y >> (depth - d)) & 1) * 2
            + ((p.z >> (depth - d)) & 1)) as usize;

        self.nodes[node].children[i].0 = if d < limit {
            let child = if let Some(child) = self.nodes[node].children[i].1 {
                child
            } else {
                let child = OctreeIndex::new(self.nodes.len() as u16).unwrap();
                self.nodes.push(Node {
                    children: array::from_fn(|_| (T::default(), None)),
                });
                self.nodes[node].children[i].1 = Some(child);
                child
            };

            self.insert_rec(p, depth, limit, value, d + 1, child.get() as usize)
        } else {
            value
        };

        let values = array::from_fn(|i| &self.nodes[node].children[i].0);

        (self.merger)(values)
    }
}
