use habanero_physics::world::topology::WorldSpace;
use jalapeno::math::{
    point::Point,
    topology::{Space, XyzVector},
};

use super::Scene;

// Relative screen-voxel space
pub struct ScreenSpace;

impl ScreenSpace {
    pub fn from_world_space(p: Point<WorldSpace, 3>) -> Option<Point<ScreenSpace, 3>> {
        if p.x >= 0
            && p.y >= 0
            && p.z >= 0
            && p.x < Scene::DIMENSION
            && p.y < Scene::DIMENSION
            && p.z < Scene::DIMENSION
        {
            let p = Point::<ScreenSpace, 3>(p.0.map(|i| i as u32));
            Some(p)
        } else {
            None
        }
    }
}

impl Space<3> for ScreenSpace {
    type Coordinate = u32;
    type Coordinates = XyzVector<u32>;

    fn as_coordinates(src: &[Self::Coordinate; 3]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 3]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}
