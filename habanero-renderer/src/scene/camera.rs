use habanero_physics::{forward, right, up};
use jalapeno::math::{quaterion::Quaterion, Matrix3, Matrix4, Vector3};

#[derive(Debug, Default)]
pub struct Camera {
    pub position: Vector3,
    pub rotation: Vector3,
}

impl Camera {
    pub fn build_view_matrix(&self) -> Matrix4 {
        let rotation = Quaterion::from_rotations(self.rotation);
        let rot = Matrix3::from_rotation(rotation);
        let pos = Matrix4::from_translation(-self.position);
        pos * Matrix4::from(rot)
    }

    // Builds the (right, forward, up) vectors
    pub fn build_view_frame(&self) -> (Vector3, Vector3, Vector3) {
        let rotation = Quaterion::from_rotations(self.rotation);
        let right = rotation.rotate(right());
        let forward = rotation.rotate(forward());
        let up = rotation.rotate(up());

        (right, forward, up)
    }
}
