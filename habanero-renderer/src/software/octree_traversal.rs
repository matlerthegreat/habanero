use crate::scene::octree::Octree;
use habanero_physics::world::voxel::VoxelColor;
use jalapeno::math::{vector::Vector, IVector3, Scalar, Vector3};

use super::raytracing::Ray;

//type UVector3 = TVector<u32>;
//type BVector3 = TVector<bool>;
type UVector3 = IVector3;

fn signum(v: Vector3) -> Vector3 {
    Vector(v.0.map(Scalar::signum))
}

fn com_mul(v: Vector3, w: Vector3) -> Vector3 {
    Vector(std::array::from_fn(|i| v[i] * w[i]))
}

fn com_div(v: Vector3, w: Vector3) -> Vector3 {
    Vector(std::array::from_fn(|i| v[i] / w[i]))
}

fn max(v: Vector3) -> Scalar {
    v.into_iter()
        .reduce(|a, b| if a > b { a } else { b })
        .unwrap()
}

fn min(v: Vector3) -> Scalar {
    v.into_iter()
        .reduce(|a, b| if a < b { a } else { b })
        .unwrap()
}

fn arg_max(Vector([x, y, z]): Vector3) -> usize {
    if x >= y {
        if x >= z {
            0
        } else {
            2
        }
    } else if y >= z {
        1
    } else {
        2
    }
}

fn get_subnode_index_offset(mut subnode: UVector3, a: Vector3) -> usize {
    if a.x < 0.0 {
        subnode.x = !subnode.x;
    }
    if a.y < 0.0 {
        subnode.y = !subnode.y;
    }
    if a.z < 0.0 {
        subnode.z = !subnode.z;
    }

    (subnode.x as usize & 1) * 4 + (subnode.y as usize & 1) * 2 + (subnode.z as usize & 1)
}

fn is_out_of_bounds(next_node: UVector3, current_node: UVector3) -> bool {
    for i in 0..3 {
        if (next_node[i] & current_node[i]) != 0 {
            return true;
        }
    }
    false
}

fn get_first_child_node(t0: Vector3, t1: Vector3, hit: &mut Vector3) -> UVector3 {
    let d = t1 - t0;
    let tm = t0 + (d / 2.0);
    let mut child: IVector3 = Vector([0; 3]);

    // The furthest the ray has to travel to enter the voxel (t0)
    // marks the last boudary it will pass
    // which also is the entry face of the voxel

    match arg_max(t0) {
        0 => {
            // Ray comming in from X direction

            if tm.y < t0.x {
                child.y = 1;
            }
            if tm.z < t0.x {
                child.z = 1;
            }
            *hit = Vector([t0.x, 0.0, 0.0]);
        }
        1 => {
            // Ray comming in from Y direction

            if tm.x < t0.y {
                child.x = 1;
            }
            if tm.z < t0.y {
                child.z = 1;
            }
            *hit = Vector([0.0, t0.y, 0.0]);
        }
        2 => {
            // Ray comming in from Z direction

            if tm.x < t0.z {
                child.x = 1;
            }
            if tm.y < t0.z {
                child.y = 1;
            }
            *hit = Vector([0.0, 0.0, t0.z]);
        }
        _ => unreachable!(),
    }

    child
}

fn get_next_node(t1: Vector3) -> IVector3 {
    // The shortest the ray has to travel to exit the voxel (t1)
    // marks the first boudary it will pass
    // which also is the exit face of the voxel

    if (t1.z <= t1.x) && (t1.z <= t1.y) {
        // Exit plane XY
        Vector([0, 0, 1])
    } else if (t1.y <= t1.x) && (t1.y <= t1.z) {
        // Exit plane XZ
        Vector([0, 1, 0])
    } else {
        // Exit plane YZ
        Vector([1, 0, 0])
    }
}

fn update_parameters_for_subnode(t0: &mut Vector3, t1: &mut Vector3, subnode: UVector3) {
    let d = *t1 - *t0;
    let tm = *t0 + (d / 2.0);

    if subnode.x != 0 {
        t0.x = tm.x;
    } else {
        t1.x = tm.x;
    }

    if subnode.y != 0 {
        t0.y = tm.y;
    } else {
        t1.y = tm.y;
    }

    if subnode.z != 0 {
        t0.z = tm.z;
    } else {
        t1.z = tm.z;
    }
}

fn update_parameters_for_up_node(t0: &mut Vector3, t1: &mut Vector3, subnode: UVector3) {
    let d = *t1 - *t0;

    if (subnode.x & 1) != 0 {
        t0.x -= d.x;
    } else {
        t1.x += d.x;
    }

    if (subnode.y & 1) != 0 {
        t0.y -= d.y;
    } else {
        t1.y += d.y;
    }
    if (subnode.z & 1) != 0 {
        t0.z -= d.z;
    } else {
        t1.z += d.z;
    }
}

fn update_parameters_for_next_node(
    t0: &mut Vector3,
    t1: &mut Vector3,
    hit: &mut Vector3,
    subnode: UVector3,
) {
    let d = *t1 - *t0;

    if subnode.x != 0 {
        t0.x = t1.x;
        t1.x += d.x;
        *hit = Vector([t0.x, 0.0, 0.0]);
    }
    if subnode.y != 0 {
        t0.y = t1.y;
        t1.y += d.y;
        *hit = Vector([0.0, t0.y, 0.0]);
    }
    if subnode.z != 0 {
        t0.z = t1.z;
        t1.z += d.z;
        *hit = Vector([0.0, 0.0, t0.z]);
    }
}

pub(super) fn traverse_octree(octree: &Octree<VoxelColor>, ray: Ray) -> Option<Vector3> {
    let a = signum(ray.direction);
    let origin = com_mul(a, ray.origin);
    let direction = com_mul(a, ray.direction);

    let bounds = Vector([1.0; 3]);

    let mut t0 = com_div(-bounds - origin, direction);
    let mut t1 = com_div(bounds - origin, direction);
    let mut hit = Vector([0.0; 3]);

    if max(t0) >= min(t1) {
        None
    } else {
        let mut subnode = get_first_child_node(t0, t1, &mut hit);
        update_parameters_for_subnode(&mut t0, &mut t1, subnode);

        let mut generations = vec![0u16];

        while let Some(&siblings) = generations.last() {
            let index = get_subnode_index_offset(subnode, a);

            // TODO: Rays originating from inside the octree should maybe check for negative t0
            match octree.get(siblings as usize, index) {
                (0, None) => {
                    let next_node = get_next_node(t1);

                    while is_out_of_bounds(next_node, subnode) {
                        update_parameters_for_up_node(&mut t0, &mut t1, subnode);
                        subnode /= 2;
                        generations.pop();
                    }

                    subnode += next_node;
                    update_parameters_for_next_node(&mut t0, &mut t1, &mut hit, next_node);
                }
                (_, None) => {
                    let len = hit.length();
                    let normal = -hit / len;
                    return Some(com_mul(a, normal));
                }
                (_, Some(children)) => {
                    generations.push(children.get());

                    let first_subnode = get_first_child_node(t0, t1, &mut hit);
                    subnode *= 2;
                    subnode += first_subnode;

                    update_parameters_for_subnode(&mut t0, &mut t1, first_subnode);
                }
            }
        }

        None
    }
}
