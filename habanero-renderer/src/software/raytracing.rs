use jalapeno::math::{topology::Color, vector::Vector, Vector3};

use crate::scene::Scene;

use super::octree_traversal::traverse_octree;

#[derive(Debug, Clone, Copy)]
pub(super) struct Ray {
    pub origin: Vector3,
    pub direction: Vector3,
}

pub fn render(width: u32, height: u32, scene: &Scene, output: &mut [u32]) {
    let (right, forward, up) = scene.camera.build_view_frame();
    output.iter_mut().enumerate().for_each(|(i, c)| {
        let (x, y) = (i as u32 % width, height - i as u32 / width);
        let horizontal_coefficient = (x as f32 - width as f32 / 2.0) / width as f32;
        let vertical_coefficient = (y as f32 - height as f32 / 2.0) / width as f32;

        let ray = Ray {
            direction: (forward + horizontal_coefficient * right + vertical_coefficient * up)
                .normalized(),
            origin: scene.camera.position,
        };

        if let Some(normal) = traverse_octree(&scene.octree, ray) {
            let brightness = normal.dot(scene.light);
            let color: Vector3 = brightness * Vector([0.75, 0.75, 0.25]);
            let color = 255.0 * color;
            let color: Vector<Color, 3> = Vector([color.x as u8, color.y as u8, color.z as u8]);
            *c = ((color.r as u32) << 16) | ((color.g as u32) << 8) | (color.b as u32);
        } else {
            *c = 0;
        }
    });
}
