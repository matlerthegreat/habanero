use std::{
    iter,
    sync::{Arc, RwLock},
};

use anyhow::Result;

use wgpu::{
    Color, CommandEncoderDescriptor, ComputePassDescriptor, Device, DeviceDescriptor, Features,
    Limits, LoadOp, Operations, PowerPreference, Queue, RenderPassColorAttachment,
    RenderPassDescriptor, RequestAdapterOptions, Surface, SurfaceConfiguration,
};
use winit::{dpi::PhysicalSize, window::Window};

use crate::scene::Scene;

use self::compute::{CameraBuffer, OctreeBuffer};

mod compute;
mod rasterizer;
mod wgsl;

pub struct Renderer {
    scene: Arc<RwLock<Scene>>,
    surface: Surface<'static>,
    device: Device,
    queue: Queue,
    config: SurfaceConfiguration,
    camera_buffer: CameraBuffer,
    raytracer: compute::Raytracer,
    rasterizer: rasterizer::Quad,
}

impl Renderer {
    pub async fn new(window: Arc<Window>, scene: Arc<RwLock<Scene>>) -> Self {
        let size = window.inner_size();

        let instance = wgpu::Instance::default();
        let surface = instance.create_surface(window).unwrap();

        let adapter = instance
            .request_adapter(&RequestAdapterOptions {
                power_preference: PowerPreference::default(),
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .await
            .unwrap();

        let (device, queue) = adapter
            .request_device(
                &DeviceDescriptor {
                    label: Some("Habanero device"),
                    required_features: Features::BUFFER_BINDING_ARRAY,
                    required_limits: Limits::default(),
                },
                None,
            )
            .await
            .unwrap();

        let config = surface
            .get_default_config(&adapter, size.width, size.height)
            .unwrap();
        surface.configure(&device, &config);

        let texture_desc = wgpu::TextureDescriptor {
            label: None,
            size: wgpu::Extent3d {
                width: 800,
                height: 600,
                depth_or_array_layers: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Rgba8Unorm,
            usage: wgpu::TextureUsages::STORAGE_BINDING | wgpu::TextureUsages::TEXTURE_BINDING,
            view_formats: &[],
        };
        let texture = device.create_texture(&texture_desc);
        let texture_view = texture.create_view(&Default::default());

        let octree_buffer = {
            let scene = scene.read().unwrap();
            OctreeBuffer::new(&device, &scene.octree)
        };

        let camera_buffer = CameraBuffer::new(&device);

        let raytracer =
            compute::Raytracer::new(&device, &texture_view, &octree_buffer, &camera_buffer);
        let sampler = device.create_sampler(&Default::default());
        let rasterizer = rasterizer::Quad::new(&device, &texture_view, &sampler, &config);
        Self {
            scene,
            surface,
            device,
            queue,
            config,
            camera_buffer,
            raytracer,
            rasterizer,
        }
    }

    pub fn resize(&mut self, new_size: PhysicalSize<u32>) {
        if new_size.width > 0 && new_size.height > 0 {
            self.config.width = new_size.width;
            self.config.height = new_size.height;
            self.surface.configure(&self.device, &self.config);
        }
    }

    pub fn update(&mut self) {
        let scene = self.scene.read().unwrap();
        self.camera_buffer.update(&self.queue, &scene.camera);
    }

    pub fn render(&mut self) -> Result<()> {
        let output = self.surface.get_current_texture()?;
        let view = output.texture.create_view(&Default::default());

        let mut encoder = self
            .device
            .create_command_encoder(&CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        // Compute pass
        {
            let mut compute_pass = encoder.begin_compute_pass(&ComputePassDescriptor {
                label: None,
                timestamp_writes: None,
            });

            self.raytracer.compute(&mut compute_pass);
        }

        // Render Pass
        {
            let mut render_pass = encoder.begin_render_pass(&RenderPassDescriptor {
                label: Some("Render Pass"),
                color_attachments: &[Some(RenderPassColorAttachment {
                    view: &view,
                    resolve_target: None,
                    ops: Operations {
                        load: LoadOp::Clear(Color {
                            r: 0.1,
                            g: 0.2,
                            b: 0.3,
                            a: 1.0,
                        }),
                        store: wgpu::StoreOp::Store,
                    },
                })],
                depth_stencil_attachment: None,
                timestamp_writes: None,
                occlusion_query_set: None,
            });

            self.rasterizer.render(&mut render_pass);
        }

        self.queue.submit(iter::once(encoder.finish()));
        output.present();

        Ok(())
    }
}
