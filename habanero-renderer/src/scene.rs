use std::sync::{Arc, RwLock};

use habanero_physics::{
    forward, right, up,
    world::{chunk::Chunk, topology::WorldSpace, voxel::VoxelColor, World},
};
use jalapeno::math::{point::Point, topology::Euclidean, vector::Vector, Vector3};

use self::{camera::Camera, octree::Octree, topology::ScreenSpace};

pub mod camera;
pub mod octree;
pub mod topology;

pub struct Scene {
    pub camera: Camera,
    pub light: Vector3,
    pub octree: Octree<VoxelColor>,
    pub world: Arc<RwLock<World>>,
}

fn merger(colors: [&VoxelColor; 8]) -> VoxelColor {
    *colors.into_iter().reduce(|a, b| a.max(b)).unwrap()
}

impl Scene {
    const OCTREE_DEPTH: usize = 16;
    pub const DIMENSION: i32 = 1 << Self::OCTREE_DEPTH;
    pub const RADIUS: i32 = Self::DIMENSION >> 1;

    pub fn new(world: Arc<RwLock<World>>) -> Self {
        let camera = Camera {
            position: Vector([0.0, -1.0, 0.0]),
            ..Default::default()
        };

        let light = (0.9 * up::<_, Euclidean>() - 0.6 * forward() + 0.3 * right()).normalized();

        let octree = Octree::new(merger);

        let mut scene = Self {
            camera,
            light,
            octree,
            world,
        };

        println!("Building the octree...");

        scene.build_octree();

        println!("Octree nodes count: {}", scene.octree.size());

        scene
    }

    fn build_octree(&mut self) {
        let world = self.world.read().unwrap();
        for (chunk_key, chunk) in world.chunks.iter() {
            for (voxel_key, voxel) in chunk.iter() {
                let chunk_pos =
                    Point::<WorldSpace, 3>(chunk_key.0.map(|i| i * Chunk::DIMENSION as i32));
                let voxel_offset =
                    Vector::<WorldSpace, 3>(voxel_key.0.map(i32::try_from).map(Result::unwrap));
                let world_pos = chunk_pos + voxel_offset;

                if let Some(p) = ScreenSpace::from_world_space(world_pos) {
                    if !world.is_transparent(world_pos) && world.is_surface(world_pos) {
                        let color = voxel.color;
                        if color > 0 {
                            self.octree.insert(p, Self::OCTREE_DEPTH, color);
                        }
                    }
                }
            }
        }
    }
}
