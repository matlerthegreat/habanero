use jalapeno::math::Scalar;

#[repr(C, align(16))]
pub(crate) struct Vector3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

#[repr(C, align(16))]
pub(crate) struct Vector4 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
    pub w: f32,
}

#[repr(C, align(16))]
pub(crate) struct Matrix3 {
    pub x: Vector3,
    pub y: Vector3,
    pub z: Vector3,
}

#[repr(C, align(16))]
pub(crate) struct Matrix4 {
    pub x: Vector4,
    pub y: Vector4,
    pub z: Vector4,
    pub w: Vector4,
}

impl From<[Scalar; 3]> for Vector3 {
    fn from([x, y, z]: [Scalar; 3]) -> Self {
        Self { x, y, z }
    }
}

impl From<[Scalar; 4]> for Vector4 {
    fn from([x, y, z, w]: [Scalar; 4]) -> Self {
        Self { x, y, z, w }
    }
}

impl From<jalapeno::math::Vector3> for Vector3 {
    fn from(jalapeno::math::vector::Vector([x, y, z]): jalapeno::math::Vector3) -> Self {
        Self { x, y, z }
    }
}

impl From<jalapeno::math::Vector4> for Vector4 {
    fn from(jalapeno::math::vector::Vector([x, y, z, w]): jalapeno::math::Vector4) -> Self {
        Self { x, y, z, w }
    }
}

impl From<jalapeno::math::Matrix3> for Matrix3 {
    fn from(value: jalapeno::math::Matrix3) -> Self {
        Self {
            x: value[0].into(),
            y: value[1].into(),
            z: value[2].into(),
        }
    }
}

impl From<jalapeno::math::Matrix4> for Matrix4 {
    fn from(value: jalapeno::math::Matrix4) -> Self {
        Self {
            x: value[0].into(),
            y: value[1].into(),
            z: value[2].into(),
            w: value[3].into(),
        }
    }
}
