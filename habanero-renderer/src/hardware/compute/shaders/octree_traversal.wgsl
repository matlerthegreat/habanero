/*
    Nodes structure:
    Node can either be a leaf or point to 8 children

    16 bit: color (4bit RGBA)
    16 bit: children offset (0 if node is a leaf)
*/

struct Ray {
    direction: vec3<f32>,
    origin: vec3<f32>,
}

struct Node {
    voxels: array<Voxel, 8>,
}

struct Voxel {
    data: u32,
}

struct Camera {
    position: vec3<f32>,
    rotation: mat4x4<f32>,
}

@group(0)
@binding(0)
var color_buffer: texture_storage_2d<rgba8unorm, write>;

@group(0)
@binding(1)
var<uniform> camera: Camera;

@group(0)
@binding(2)
var<storage, read> octree: array<Node>;

// Helpers

fn vec_max(v: vec3<f32>) -> f32 {
    return max(v.x, max(v.y, v.z));
}

fn vec_min(v: vec3<f32>) -> f32 {
    return min(v.x, min(v.y, v.z));
}

fn get_voxel_children(voxel: Voxel) -> u32 {
    // Get 16 MSB
    return voxel.data >> 16;
}

fn get_voxel_color(voxel: Voxel) -> u32 {
    // Get 16 LSB
    return voxel.data & 65535;
}

fn arg_max(v: vec3<f32>) -> u32 {
    if v.x >= v.y {
        if v.x >= v.z {
            return 0u;
        } else {
            return 2u;
        }
    } else if v.y >= v.z {
        return 1u;
    } else {
        return 2u;
    }
}

fn get_subnode_index_offset(subnode_in: vec3<u32>, a: vec3<f32>) -> u32 {
    var subnode: vec3<u32> = subnode_in;
    if a.x < 0.0 {
        subnode.x = ~subnode.x;
    }
    if a.y < 0.0 {
        subnode.y = ~subnode.y;
    }
    if a.z < 0.0 {
        subnode.z = ~subnode.z;
    }

    return (subnode.x & 1u) * 4u + (subnode.y & 1u) * 2u + (subnode.z & 1u);
}

fn is_out_of_bounds(next_node: vec3<u32>, current_node: vec3<u32>) -> bool {
    let t: vec3<u32> = next_node & current_node;
    let v: vec3<u32> = vec3(0u);
    return any(v != t);
}

//
// Parameters
//

var<private> t0: vec3<f32>;
var<private> t1: vec3<f32>;
var<private> hit: vec3<f32>;

//
// Parameter updates
//

fn get_first_child_node() -> vec3<u32> {
    let d = t1 - t0;
    let tm = t0 + (d / 2.0);
    var child: vec3<u32> = vec3(0u, 0u, 0u);

    // The furthest the ray has to travel to enter the voxel (t0)
    // marks the last boudary it will pass
    // which also is the entry face of the voxel
    switch arg_max(t0) {
        case 0u: {
            // Ray comming in from X direction
            if tm.y < t0.x {
                child.y = 1u;
            }
            if tm.z < t0.x {
                child.z = 1u;
            }
            hit = vec3(t0.x, 0.0, 0.0);
        }
        case 1u: {
            // Ray comming in from Y direction
            if tm.x < t0.y {
                child.x = 1u;
            }
            if tm.z < t0.y {
                child.z = 1u;
            }
            hit = vec3(0.0, t0.y, 0.0);
        }
        default: {
            // Ray comming in from Z direction
            if tm.x < t0.z {
                child.x = 1u;
            }
            if tm.y < t0.z {
                child.y = 1u;
            }
            hit = vec3(0.0, 0.0, t0.z);
        }
    }

    return child;
}

fn get_next_node() -> vec3<u32> {
    // The shortest the ray has to travel to exit the voxel (t1)
    // marks the first boudary it will pass
    // which also is the exit face of the voxel

    if (t1.z <= t1.x) && (t1.z <= t1.y) {
        // Exit plane XY
        return vec3(0u, 0u, 1u);
    } else if (t1.y <= t1.x) && (t1.y <= t1.z) {
        // Exit plane XZ
        return vec3(0u, 1u, 0u);
    } else {
        // Exit plane YZ
        return vec3(1u, 0u, 0u);
    }
}

fn update_parameters_for_subnode(subnode: vec3<u32>) {
    let d = t1 - t0;
    let tm = t0 + (d / 2.0);

    if subnode.x != 0 {
        t0.x = tm.x;
    } else {
        t1.x = tm.x;
    }

    if subnode.y != 0 {
        t0.y = tm.y;
    } else {
        t1.y = tm.y;
    }

    if subnode.z != 0 {
        t0.z = tm.z;
    } else {
        t1.z = tm.z;
    }
}

fn update_parameters_for_up_node(subnode: vec3<u32>) {
    let d = t1 - t0;

    if (subnode.x & 1) != 0 {
        t0.x -= d.x;
    } else {
        t1.x += d.x;
    }

    if (subnode.y & 1) != 0 {
        t0.y -= d.y;
    } else {
        t1.y += d.y;
    }
    if (subnode.z & 1) != 0 {
        t0.z -= d.z;
    } else {
        t1.z += d.z;
    }
}

fn update_parameters_for_next_node(subnode: vec3<u32>) {
    let d = t1 - t0;

    if subnode.x != 0 {
        t0.x = t1.x;
        t1.x += d.x;
        hit = vec3(t0.x, 0.0, 0.0);
    }
    if subnode.y != 0 {
        t0.y = t1.y;
        t1.y += d.y;
        hit = vec3(0.0, t0.y, 0.0);
    }
    if subnode.z != 0 {
        t0.z = t1.z;
        t1.z += d.z;
        hit = vec3(0.0, 0.0, t0.z);
    }
}

fn traverse_octree(ray: Ray) -> bool {
    // Set constants
    let a = sign(ray.direction);
    let origin = a * ray.origin;
    let direction = a * ray.direction;
    let bounds = vec3<f32>(1024.0);

    // Initialize parameters
    t0 = (-bounds - origin) / direction;
    t1 = (bounds - origin) / direction;

    if vec_max(t0) >= vec_min(t1) {
        return false;
    }

    // Start traversal, subnode is the binary path to the currently visiting voxel
    // Each step into the octree, the subnode path is shifted to the left to keep history
    var subnode: vec3<u32> = get_first_child_node();
    update_parameters_for_subnode(subnode);

    // Keep history of visited "generations" - levels of the octree
    var generations: array<u32, 32>;
    generations[0] = 0u;
    var gen_index: i32 = 0;

    while gen_index >= 0 {
        let generation = generations[gen_index];

        // Step into the current generation to find the current voxel
        let index = get_subnode_index_offset(subnode, a);
        let voxel = octree[generation].voxels[index];

        let children = get_voxel_children(voxel);
        if children == 0 {
            // Voxel has no children
            let color = get_voxel_color(voxel);
            if color != 0 {
                // Voxel is not fully transparent so treat is as opaque
                hit *= a;
                return true;
            } else {
                // Voxel is fully transparent, move to the next one
                let next_node = get_next_node();

                // Move up the octree if the next node is not in the siblings
                while is_out_of_bounds(next_node, subnode) {
                    update_parameters_for_up_node(subnode);
                    subnode = subnode >> vec3(1u);
                    gen_index -= 1;
                }

                subnode += next_node;
                update_parameters_for_next_node(next_node);
            }
        } else {
            // Voxel has children, step down into the next generation
            gen_index += 1;
            generations[gen_index] = children;

            // Find the first child to visit
            let first_subnode = get_first_child_node();
            subnode = subnode << vec3(1u);
            subnode += first_subnode;

            update_parameters_for_subnode(first_subnode);
        }
    }

    return false;
}


@compute
@workgroup_size(8,8,1)
fn main(@builtin(global_invocation_id) GlobalInvocationID : vec3<u32>) {

    let screen_size: vec2<i32> = vec2<i32>(textureDimensions(color_buffer));
    let screen_pos : vec2<i32> = vec2<i32>(i32(GlobalInvocationID.x), i32(GlobalInvocationID.y));

    if (screen_pos.x >= screen_size.x || screen_pos.y >= screen_size.y) {
        return;
    }

    let horizontal_coefficient: f32 = (f32(screen_pos.x) - f32(screen_size.x) / 2.0) / f32(screen_size.x);
    let vertical_coefficient: f32 = (f32(screen_pos.y) - f32(screen_size.y) / 2.0) / f32(screen_size.x);

    let right: vec3<f32> = vec3<f32>(1.0, 0.0, 0.0);
    let forwards: vec3<f32> = vec3<f32>(0.0, 1.0, 0.0);
    let up: vec3<f32> = vec3<f32>(0.0, 0.0, 1.0);

    var ray: Ray;
    //ray.direction = normalize(forwards + horizontal_coefficient * right - vertical_coefficient * up);
    //ray.origin = camera * vec3<f32>(-1.51, -10.0, -2.03);

    let direction = camera.rotation * vec4(normalize(forwards + horizontal_coefficient * right - vertical_coefficient * up), 1.0);
    ray.direction = direction.xyz;

    ray.origin = camera.position;

    var pixel_color : vec3<f32> = vec3<f32>(f32(screen_pos.x) / f32(screen_size.x), f32(screen_pos.y) / f32(screen_size.y), 0.25);

    let light = normalize((0.9 * up - 0.6 * forwards + 0.3 * right));

    if (traverse_octree(ray)) {
        let len = length(hit);
        let normal = -hit / len;
        let brightness = dot(normal, light);

        pixel_color = vec3<f32>(0.0, 1.0, 0.0) * brightness;
    }

    textureStore(color_buffer, screen_pos, vec4<f32>(pixel_color, 1.0));
}