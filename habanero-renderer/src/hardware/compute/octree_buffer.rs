use habanero_physics::world::voxel::VoxelColor;
use wgpu::util::DeviceExt;

use crate::scene::octree::Octree;

pub struct OctreeBuffer {
    buffer: wgpu::Buffer,
}

impl OctreeBuffer {
    pub fn new(device: &wgpu::Device, octree: &Octree<VoxelColor>) -> Self {
        let contents = octree.get_raw();
        let buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Octree buffer"),
            contents,
            usage: wgpu::BufferUsages::STORAGE,
        });

        Self { buffer }
    }

    pub(crate) fn as_binding(&self) -> wgpu::BufferBinding {
        wgpu::BufferBinding {
            offset: 0,
            size: None,
            buffer: &self.buffer,
        }
    }
}
