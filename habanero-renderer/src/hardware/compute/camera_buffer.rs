use std::{mem, slice};

use jalapeno::math::{quaterion::Quaterion, Matrix3, Matrix4};

use crate::{hardware::wgsl, scene::camera::Camera};

#[repr(C)]
struct RawCameraBufferData {
    position: wgsl::Vector3,
    rotation: wgsl::Matrix4,
}

pub struct CameraBuffer {
    buffer: wgpu::Buffer,
}

impl CameraBuffer {
    pub fn new(device: &wgpu::Device) -> Self {
        let buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("CameraBuffer"),
            size: mem::size_of::<RawCameraBufferData>() as u64,
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            mapped_at_creation: false,
        });

        Self { buffer }
    }

    pub fn update(&mut self, queue: &wgpu::Queue, camera: &Camera) {
        let rotation = Quaterion::from_rotations(camera.rotation);
        let rotation: Matrix4 = Matrix3::from_rotation(rotation).into();
        let raw_data = RawCameraBufferData {
            position: camera.position.into(),
            rotation: rotation.into(),
        };

        let data = unsafe {
            let len = mem::size_of::<RawCameraBufferData>();
            let data = (&raw_data) as *const RawCameraBufferData as *const u8;
            slice::from_raw_parts(data, len)
        };

        queue.write_buffer(&self.buffer, 0, data);
    }

    pub(super) fn as_binding(&self) -> wgpu::BufferBinding {
        wgpu::BufferBinding {
            offset: 0,
            size: None,
            buffer: &self.buffer,
        }
    }
}
