use std::{marker::PhantomData, mem, slice};

use jalapeno::math::{Matrix3, Matrix4, Quaterion, Vector3};
use wgpu::util::DeviceExt;

pub trait InstanceData: Sized {
    type RawData: for<'a> From<&'a Self>;

    const ATTRIBUTESS: &'static [wgpu::VertexAttribute];
    const LAYOUT: wgpu::VertexBufferLayout<'static> = wgpu::VertexBufferLayout {
        array_stride: mem::size_of::<Self::RawData>() as wgpu::BufferAddress,
        step_mode: wgpu::VertexStepMode::Instance,
        attributes: Self::ATTRIBUTESS,
    };
}

#[derive(Debug, Clone)]
pub struct TransformInstanceData {
    pub position: Vector3,
    pub rotation: Quaterion,
}

impl TransformInstanceData {
    pub fn new(position: Vector3, rotation: Quaterion) -> Self {
        Self { position, rotation }
    }
}

impl InstanceData for TransformInstanceData {
    type RawData = [[f32; 4]; 4];

    const ATTRIBUTESS: &'static [wgpu::VertexAttribute] = &wgpu::vertex_attr_array![
        0 => Float32x4,
        1 => Float32x4,
        2 => Float32x4,
        3 => Float32x4];
}

impl From<&TransformInstanceData> for [[f32; 4]; 4] {
    fn from(value: &TransformInstanceData) -> Self {
        let translation = Matrix4::from_translation(value.position);
        let rotation = Matrix4::from(Matrix3::from_rotation(value.rotation));

        let transform = translation * rotation;
        transform.into()
    }
}

pub struct InstanceBuffer<InstanceDataType: InstanceData> {
    pub buffer: wgpu::Buffer,
    _phantom: PhantomData<InstanceDataType>,
}

impl<InstanceDataType: InstanceData> InstanceBuffer<InstanceDataType> {
    pub fn new(device: &wgpu::Device, instances_raw_data: &[InstanceDataType::RawData]) -> Self {
        let name = "Instance";

        // For an empty buffer
        /*let raw_data_stride = mem::size_of::<InstanceDataType::RawData>();

        let buffer = device.create_buffer(&BufferDescriptor {
            size: (raw_data_stride * n) as u64,
            usage: BufferUsages::VERTEX,
            mapped_at_creation: false,
            label: Some(&format!("{name} Instance Buffer")),
        });*/

        let contents = unsafe {
            let len = mem::size_of_val(instances_raw_data);
            slice::from_raw_parts(instances_raw_data.as_ptr() as *const u8, len)
        };
        let buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some(&format!("{name} Buffer")),
            contents,
            usage: wgpu::BufferUsages::VERTEX,
        });

        Self {
            buffer,
            _phantom: PhantomData,
        }
    }
}
