use std::{marker::PhantomData, mem, slice};

use wgpu::{
    util::{BufferInitDescriptor, DeviceExt},
    Buffer, BufferUsages, Device,
};

use super::vertex::Vertex;

pub struct Mesh<VertexType: Vertex> {
    pub vertex_buffer: Buffer,
    _phantom: PhantomData<VertexType>,
}

impl<VertexType: Vertex> Mesh<VertexType> {
    pub fn new(device: &Device, vertices: &[VertexType::RawData]) -> Self {
        let contents = unsafe {
            let len = mem::size_of_val(vertices);
            slice::from_raw_parts(vertices.as_ptr() as *const u8, len)
        };
        let vertex_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Vertex Buffer"),
            contents,
            usage: BufferUsages::VERTEX,
        });

        Self {
            vertex_buffer,
            _phantom: PhantomData,
        }
    }
}
