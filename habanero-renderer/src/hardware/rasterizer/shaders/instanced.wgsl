// Vertex shader

struct CameraUniform {
    view_proj: mat4x4<f32>,
};
@group(0) @binding(0)
var<uniform> camera: CameraUniform;

struct VertexInput {
    @location(0) position: vec3<f32>,
    @location(1) color: vec3<f32>,
    @location(2) world_matrix_0: vec4<f32>,
    @location(3) world_matrix_1: vec4<f32>,
    @location(4) world_matrix_2: vec4<f32>,
    @location(5) world_matrix_3: vec4<f32>,
};

struct VertexOutput {
    @builtin(position) position: vec4<f32>,
    @location(0) color: vec3<f32>,
};

@vertex
fn vs_main(
    input: VertexInput,
) -> VertexOutput {
    let world = mat4x4<f32>(
        input.world_matrix_0,
        input.world_matrix_1,
        input.world_matrix_2,
        input.world_matrix_3,
    );

    var out: VertexOutput;
    out.color = input.color;
    out.position = camera.view_proj * world * vec4<f32>(input.position, 1.0);
    return out;
}

// Fragment shader

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    return vec4<f32>(in.color, 1.0);
}