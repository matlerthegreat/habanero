use std::mem;

use wgpu::{
    BindGroup, BindGroupDescriptor, BindGroupEntry, BindGroupLayout, BindGroupLayoutDescriptor,
    BindGroupLayoutEntry, BindingType, Buffer, BufferBindingType, BufferDescriptor, BufferUsages,
    Device, ShaderStages,
};

pub trait Uniform {
    fn bind_group_layout(&self) -> &BindGroupLayout;
}

pub struct CameraUniform {
    pub bind_group_layout: BindGroupLayout,
    pub bind_group: BindGroup,
    pub buffer: Buffer,
}

impl CameraUniform {
    pub fn new(device: &Device) -> Self {
        let name = "Camera";

        let buffer = device.create_buffer(&BufferDescriptor {
            size: mem::size_of::<CameraUniform>() as u64,
            usage: BufferUsages::UNIFORM | BufferUsages::COPY_DST,
            mapped_at_creation: false,
            label: Some(&format!("{name} Uniform Buffer")),
        });

        let bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            entries: &[BindGroupLayoutEntry {
                binding: 0,
                visibility: ShaderStages::VERTEX,
                ty: BindingType::Buffer {
                    ty: BufferBindingType::Uniform,
                    has_dynamic_offset: false,
                    min_binding_size: None,
                },
                count: None,
            }],
            label: Some(&format!("{name} Render Bind Group Layout")),
        });

        let bind_group = device.create_bind_group(&BindGroupDescriptor {
            layout: &bind_group_layout,
            entries: &[BindGroupEntry {
                binding: 0,
                resource: buffer.as_entire_binding(),
            }],
            label: Some(&format!("{name} Render Bind Group")),
        });

        Self {
            buffer,
            bind_group_layout,
            bind_group,
        }
    }
}

impl Uniform for CameraUniform {
    fn bind_group_layout(&self) -> &BindGroupLayout {
        &self.bind_group_layout
    }
}
