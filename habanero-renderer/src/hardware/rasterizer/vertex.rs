use std::mem;

use jalapeno::math::Vector3;
use wgpu::{vertex_attr_array, BufferAddress, VertexAttribute, VertexBufferLayout, VertexStepMode};

pub trait Vertex: Sized {
    type RawData: for<'a> From<&'a Self>;

    const ATTRIBUTESS: &'static [VertexAttribute];
    const LAYOUT: VertexBufferLayout<'static> = VertexBufferLayout {
        array_stride: mem::size_of::<Self::RawData>() as BufferAddress,
        step_mode: VertexStepMode::Vertex,
        attributes: Self::ATTRIBUTESS,
    };
}

pub struct PosVertex {
    pub position: Vector3,
}

impl Vertex for PosVertex {
    type RawData = [f32; 3];
    const ATTRIBUTESS: &'static [VertexAttribute] = &vertex_attr_array![0 => Float32x3];
}

impl From<&PosVertex> for [f32; 3] {
    fn from(value: &PosVertex) -> Self {
        value.position.into()
    }
}

pub struct PosColVertex {
    pub position: Vector3,
    pub color: Vector3,
}

#[repr(C)]
pub struct RawPosColVertex {
    pub position: [f32; 3],
    pub color: [f32; 3],
}

impl Vertex for PosColVertex {
    type RawData = RawPosColVertex;
    const ATTRIBUTESS: &'static [VertexAttribute] =
        &vertex_attr_array![0 => Float32x3, 1 => Float32x3];
}

impl From<&PosColVertex> for RawPosColVertex {
    fn from(value: &PosColVertex) -> Self {
        let position = value.position.into();
        let color = value.color.into();

        Self { position, color }
    }
}
