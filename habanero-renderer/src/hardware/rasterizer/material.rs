use std::marker::PhantomData;

use wgpu::{
    include_wgsl, BlendComponent, BlendState, ColorTargetState, ColorWrites, Device, Face,
    FragmentState, FrontFace, MultisampleState, PipelineLayoutDescriptor, PolygonMode,
    PrimitiveState, PrimitiveTopology, RenderPipeline, RenderPipelineDescriptor,
    SurfaceConfiguration, VertexAttribute, VertexState,
};

use super::{instance::InstanceData, uniform::Uniform, vertex::Vertex};

pub struct Material<VertexType: Vertex> {
    pub render_pipeline: RenderPipeline,
    _phantom_vertex: PhantomData<VertexType>,
}

impl<VertexType: Vertex> Material<VertexType> {
    pub fn new(device: &Device, config: &SurfaceConfiguration, uniform: &impl Uniform) -> Self {
        let name = "Simple";
        let shader = device.create_shader_module(include_wgsl!("shaders/simple.wgsl"));

        let render_pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: Some(&format!("{name} Render Pipeline Layout")),
            bind_group_layouts: &[(uniform.bind_group_layout())],
            push_constant_ranges: &[],
        });

        let render_pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
            label: Some(&format!("{name} Render Pipeline")),
            layout: Some(&render_pipeline_layout),
            vertex: VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[VertexType::LAYOUT],
            },
            fragment: Some(FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &[Some(ColorTargetState {
                    format: config.format,
                    blend: Some(BlendState {
                        color: BlendComponent::REPLACE,
                        alpha: BlendComponent::REPLACE,
                    }),
                    write_mask: ColorWrites::ALL,
                })],
            }),
            primitive: PrimitiveState {
                topology: PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: FrontFace::Ccw,
                cull_mode: Some(Face::Back),
                polygon_mode: PolygonMode::Fill,
                unclipped_depth: false,
                conservative: false,
            },
            depth_stencil: None,
            multisample: MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            multiview: None,
        });

        Self {
            render_pipeline,
            _phantom_vertex: PhantomData,
        }
    }
}

pub struct InstancedMaterial<VertexType: Vertex, InstanceType> {
    pub render_pipeline: RenderPipeline,
    _phantom_vertex: PhantomData<(VertexType, InstanceType)>,
}

impl<VertexType: Vertex, InstanceType: InstanceData> InstancedMaterial<VertexType, InstanceType> {
    pub fn new(device: &Device, config: &SurfaceConfiguration, uniform: &impl Uniform) -> Self {
        let name = "Instanced Material";
        let shader = device.create_shader_module(include_wgsl!("shaders/instanced.wgsl"));

        let render_pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: Some(&format!("{name} Render Pipeline Layout")),
            bind_group_layouts: &[(uniform.bind_group_layout())],
            push_constant_ranges: &[],
        });

        let vertex_layout_attributes_count = VertexType::ATTRIBUTESS.len() as u32;
        let instance_layout_attributes: Vec<_> = InstanceType::ATTRIBUTESS
            .iter()
            .cloned()
            .map(|attribute| VertexAttribute {
                shader_location: attribute.shader_location + vertex_layout_attributes_count,
                ..attribute
            })
            .collect();
        let instance_layout = wgpu::VertexBufferLayout {
            attributes: &instance_layout_attributes,
            ..InstanceType::LAYOUT
        };

        let render_pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
            label: Some(&format!("{name} Render Pipeline")),
            layout: Some(&render_pipeline_layout),
            vertex: VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[VertexType::LAYOUT, instance_layout],
            },
            fragment: Some(FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &[Some(ColorTargetState {
                    format: config.format,
                    blend: Some(BlendState {
                        color: BlendComponent::REPLACE,
                        alpha: BlendComponent::REPLACE,
                    }),
                    write_mask: ColorWrites::ALL,
                })],
            }),
            primitive: PrimitiveState {
                topology: PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: FrontFace::Ccw,
                cull_mode: Some(Face::Back),
                polygon_mode: PolygonMode::Fill,
                unclipped_depth: false,
                conservative: false,
            },
            depth_stencil: None,
            multisample: MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            multiview: None,
        });

        Self {
            render_pipeline,
            _phantom_vertex: PhantomData,
        }
    }
}
