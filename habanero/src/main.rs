use habanero::run;
use jalapeno::futures::thread_pool::thread_pool;

fn main() {
    thread_pool(4, run)
}
