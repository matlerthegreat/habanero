use jalapeno::math::{vector::Vector, Scalar, Vector3};
use winit::{
    event::{ElementState, KeyEvent, RawKeyEvent},
    keyboard::{KeyCode, PhysicalKey},
};

pub struct Input {
    user_movement: Vector3,
    user_rotation: Vector3,
}

impl Input {
    pub fn new() -> Self {
        Self {
            user_movement: Default::default(),
            user_rotation: Default::default(),
        }
    }

    fn handle_physical_key(&mut self, physical_key: PhysicalKey, pressed: bool) {
        let value = if pressed { 1.0 } else { 0.0 };

        match physical_key {
            PhysicalKey::Code(KeyCode::KeyF) => self.user_movement.y = value,
            PhysicalKey::Code(KeyCode::KeyS) => self.user_movement.y = -value,
            PhysicalKey::Code(KeyCode::KeyT) => self.user_movement.x = value,
            PhysicalKey::Code(KeyCode::KeyR) => self.user_movement.x = -value,
            PhysicalKey::Code(KeyCode::KeyP) => self.user_movement.z = value,
            PhysicalKey::Code(KeyCode::KeyW) => self.user_movement.z = -value,
            _ => {}
        }
    }

    pub fn handle_keyboard_input(&mut self, keyboard_input: KeyEvent) {
        let pressed = match keyboard_input.state {
            ElementState::Pressed => true,
            ElementState::Released => false,
        };

        self.handle_physical_key(keyboard_input.physical_key, pressed);
    }

    pub fn handle_raw_keyboard_input(&mut self, keyboard_input: RawKeyEvent) {
        let pressed = match keyboard_input.state {
            ElementState::Pressed => true,
            ElementState::Released => false,
        };

        self.handle_physical_key(keyboard_input.physical_key, pressed);
    }

    pub fn handle_mouse_input(&mut self, (x, y): (f64, f64)) {
        self.user_rotation += Vector([-y as Scalar, 0.0, -x as Scalar]);
    }

    pub fn consume_user_movement(&mut self) -> Vector3 {
        self.user_movement
    }

    pub fn consume_user_rotation(&mut self) -> Vector3 {
        std::mem::take(&mut self.user_rotation)
    }
}
