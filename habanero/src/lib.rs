use std::sync::{Arc, RwLock};

use game::Game;
use habanero_physics::world::World;
use habanero_renderer::{hardware::Renderer, scene::Scene};
use input::Input;
use jalapeno::futures::thread_pool::Spawner;
use winit::{
    event::*,
    event_loop::EventLoop,
    keyboard::{Key, NamedKey},
    window::WindowBuilder,
};

mod game;
mod input;

pub async fn run(spawner: Spawner) {
    let event_loop = EventLoop::new().unwrap();
    let window = Arc::new(WindowBuilder::new().build(&event_loop).unwrap());

    #[cfg(target_os = "linux")]
    window
        .set_cursor_grab(winit::window::CursorGrabMode::Locked)
        .unwrap();

    window.set_cursor_visible(false);

    let main_window = window.id();

    let world = Arc::new(RwLock::new(World::new()));

    let scene = Arc::new(RwLock::new(Scene::new(world)));

    let mut renderer = Renderer::new(window, scene.clone()).await;

    let input = Arc::new(RwLock::new(Input::new()));

    let game = Game::new(scene.clone(), input.clone()).await;

    spawner.spawn(game.run());

    event_loop
        .run(move |event, control| match event {
            Event::WindowEvent { event, window_id } if window_id == main_window => match event {
                WindowEvent::CloseRequested
                | WindowEvent::KeyboardInput {
                    event:
                        KeyEvent {
                            logical_key: Key::Named(NamedKey::Escape),
                            ..
                        },
                    ..
                } => control.exit(),
                WindowEvent::KeyboardInput {
                    device_id: _,
                    event,
                    is_synthetic: _,
                } => {
                    let mut input = input.write().unwrap();
                    input.handle_keyboard_input(event);
                }
                WindowEvent::Resized(physical_size) => {
                    renderer.resize(physical_size);
                }
                _ => {}
            },
            Event::DeviceEvent {
                device_id: _,
                event: DeviceEvent::MouseMotion { delta },
            } => {
                let mut input = input.write().unwrap();
                input.handle_mouse_input(delta);
            }
            Event::DeviceEvent {
                device_id: _,
                event: DeviceEvent::Key(key),
            } => {
                let mut input = input.write().unwrap();
                input.handle_raw_keyboard_input(key);
            }
            Event::AboutToWait => {
                control.set_control_flow(winit::event_loop::ControlFlow::Poll);
                renderer.update();
                renderer.render().unwrap();
            }
            _ => {}
        })
        .unwrap();
}
