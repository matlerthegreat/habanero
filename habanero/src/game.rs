use std::{
    sync::{Arc, RwLock},
    thread,
    time::Duration,
};

use habanero_renderer::scene::Scene;
use jalapeno::{
    futures::yield_now::yield_now,
    math::{quaterion::Quaterion, Scalar},
};

use crate::input::Input;

pub struct Game {
    scene: Arc<RwLock<Scene>>,
    input: Arc<RwLock<Input>>,
}

impl Game {
    pub async fn new(scene: Arc<RwLock<Scene>>, input: Arc<RwLock<Input>>) -> Self {
        Self { scene, input }
    }

    pub async fn run(mut self) -> ! {
        let dt = 1.0 / 100.0;
        loop {
            self.update(dt).await;

            // TODO: This should be an asyncronous sleep
            thread::sleep(Duration::from_secs_f32(dt));
            yield_now().await;
        }
    }

    async fn update(&mut self, dt: Scalar) {
        // Scene modification
        {
            let mut scene = self.scene.write().unwrap();

            let mut input = self.input.write().unwrap();
            let movement = input.consume_user_movement();
            let rotation = input.consume_user_rotation();

            scene.camera.rotation += dt * rotation / 10.0;

            let rotation = Quaterion::from_rotations(scene.camera.rotation);
            let d = rotation.rotate(dt * movement);
            scene.camera.position += 50.0 * dt * d;
        }
    }
}
